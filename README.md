# Starfleet Athletics Card Template

An Inkscape SVG template for "Starfleet Athletics" cards.

Xolonium included in repo (SIL OpenFont license)

Cardstock texture from < http://www.photos-public-domain.com/2018/05/11/white-card-stock-paper-texture/ >

Delta vector from < https://commons.wikimedia.org/wiki/File:Delta-shield.svg >